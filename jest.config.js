module.exports = {
  collectCoverage: true,
  coverageDirectory: './coverage',
  coverageReporters: ['html', 'text-summary'],
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/src/tsconfig.spec.json',
    },
  },
  moduleNameMapper: {
    '@backend-bridge/(.*)$': '<rootDir>/src/app/modules/backend-bridge/$1',
    '@core/(.*)$': '<rootDir>/src/app/modules/core/$1',
    '@shared/(.*)$': '<rootDir>/src/app/modules/shared/$1',
    '@environment': '<rootDir>/src/environments/environment',
  },
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/src/jest/setup.ts'],
  transform: {
    '^.+\\.(ts|js|html)$': 'ts-jest',
  },
};
