export const api = {
  services: {
    games: {
      name: 'assets/games-list.json',
    },
  },
  prefix: 'api',
  version: 'v1',
  origin: 'https://www.gbchip.com',
  itemsPerPage: 20,
};
