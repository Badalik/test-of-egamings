export interface IGamesFilterQuery {
  categoryID?: number;
  merchant?: number;
  name?: string;
  page?: number;
  sortDirection?: string;
  sortField?: string;
}
