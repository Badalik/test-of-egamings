import { environment } from '@environment';

export abstract class BaseBridgeService {
  protected readonly apiUrl: string;

  constructor(serviceName: string, version?: string) {
    this.apiUrl = [
      // environment.api.origin,
      // environment.api.prefix,
      // version || environment.api.version,
      serviceName,
    ].join('/');
  }

  protected stringify(data: object): { [key: string]: string | undefined } {
    return Object.keys(data).reduce((stringified, key) => {
      const rawValue = data[key];
      let value: string;
      if (rawValue !== null && rawValue !== undefined) {
        value = String(data[key]);
      }
      return {
        ...stringified,
        [key]: value,
      };
    }, {});
  }
}
