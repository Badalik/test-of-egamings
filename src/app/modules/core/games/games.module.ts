import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';

import { FiltersModule } from '@shared/filters';
import { LayoutModule } from '@shared/layout';
import { PipesModule } from '@shared/pipes';
import {
  GamesCategoriesComponent,
  GamesListComponent,
  GamesTileComponent,
} from './components';
import { GamesContainerComponent } from './containers';
import { GamesRouting } from './games.router';

@NgModule({
  declarations: [
    GamesCategoriesComponent,
    GamesContainerComponent,
    GamesListComponent,
    GamesTileComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FiltersModule,
    LayoutModule,
    PipesModule,
    NgxPaginationModule,
    GamesRouting,
  ],
})
export class GamesModule {}
