import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { environment } from '@environment';

@Component({
  selector: 'games-tile',
  templateUrl: './games-tile.component.html',
  styleUrls: ['./games-tile.component.scss'],
})
export class GamesTileComponent {
  @Input() public title: string;
  @Input() public imageSRC: string;

  public environment = environment;

  constructor(private sanitizer: DomSanitizer) {}

  get image() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      `${environment.api.origin}/${this.imageSRC}`,
    );
  }
}
