import { Component, Input } from '@angular/core';

@Component({
  selector: 'games-categories',
  templateUrl: './games-categories.component.html',
  styleUrls: ['./games-categories.component.scss'],
})
export class GamesCategoriesComponent {
  @Input() public categoriesList: any[];
}
