import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { environment } from '@environment';
import { pluck } from 'rxjs/operators';

@Component({
  selector: 'games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss'],
})
export class GamesListComponent implements OnChanges, OnInit {
  @Input() public currentPage: number;
  @Input() public gamesList: any[];
  @Input() public merchantsList: any[];
  @Input() public searchGamesList;

  public category: string;
  public gamesListCategory: any[];
  public modifiedGamesList: any[];
  public perPage: number = environment.api.itemsPerPage;

  constructor(private route: ActivatedRoute, private router: Router) {}

  public ngOnChanges(changes) {
    if (changes.searchGamesList && changes.searchGamesList.currentValue) {
      const searchObj = changes.searchGamesList.currentValue;
      const searchObjValues = Object.values(searchObj);
      if (!searchObjValues.length) {
        this.modifiedGamesList = undefined;
        return;
      }
      const isCorrectObj = searchObjValues.some(
        (value: string | any[]) => value && value.length,
      );
      if (!isCorrectObj) {
        this.modifiedGamesList = undefined;
        return;
      }
      if (searchObj.categoryID) {
        const arr = [];
        if (this.modifiedGamesList) {
          searchObj.categoryID.forEach((categoryID) => {
            this.modifiedGamesList.forEach((item) => {
              if (item.CategoryID.includes(categoryID)) {
                arr.push(item);
              }
            });
          });
        } else {
          searchObj.categoryID.forEach((categoryID) => {
            this.gamesList.forEach((item) => {
              if (item.CategoryID.includes(categoryID)) {
                arr.push(item);
              }
            });
          });
        }
        this.modifiedGamesList = arr;
      }
      if (searchObj.name) {
        if (this.modifiedGamesList) {
          this.modifiedGamesList = this.modifiedGamesList.filter(
            (item) =>
              item.Name.en
                .toLowerCase()
                .indexOf(searchObj.name.toLowerCase()) !== -1,
          );
        } else {
          this.modifiedGamesList = this.gamesList.filter(
            (item) =>
              item.Name.en
                .toLowerCase()
                .indexOf(searchObj.name.toLowerCase()) !== -1,
          );
        }
      }
      if (searchObj.merchant) {
        const arr = [];
        const merchantsValues = Object.values(this.merchantsList);
        const searchMerchants = merchantsValues.filter(
          (item) =>
            item.Name.toLowerCase().indexOf(
              searchObj.merchant.toLowerCase(),
            ) !== -1,
        );
        if (!searchMerchants.length) {
          this.modifiedGamesList = [];
          return;
        }
        if (this.modifiedGamesList) {
          searchMerchants.forEach((merchant) => {
            this.modifiedGamesList.forEach((item) => {
              if (item.MerchantID === merchant.ID) {
                arr.push(item);
              }
            });
          });
        } else {
          searchMerchants.forEach((merchant) => {
            this.gamesList.forEach((item) => {
              if (item.MerchantID === merchant.ID) {
                arr.push(item);
              }
            });
          });
        }
        this.modifiedGamesList = arr;
      }
    }
  }

  public ngOnInit(): void {
    this.route.params.pipe(pluck('categoryId')).subscribe((param: string) => {
      this.category = param;
      if (param) {
        this.gamesListCategory = this.gamesList.filter((item) =>
          item.CategoryID.includes(param),
        );
      }
    });
  }

  public onPageChange(page: number) {
    if (page) {
      this.navigate(page);
    }
  }

  public get currentGamesList() {
    if (this.category) {
      return this.gamesListCategory;
    }
    if (this.modifiedGamesList) {
      return this.modifiedGamesList;
    } else {
      return this.gamesList;
    }
  }

  private navigate(page: number) {
    if (page) {
      this.router.navigate([], {
        queryParams: { page },
        queryParamsHandling: 'merge',
      });
    }
  }
}
