import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { SortDirection } from '@backend-bridge/base-bridge';
import { DynamicFilterFieldType, IDynamicFilterField } from '@shared/filters';

import { Observable } from 'rxjs';
import { map, pluck } from 'rxjs/operators';

@Component({
  selector: 'games-container',
  templateUrl: './games-container.component.html',
  styleUrls: ['./games-container.component.scss'],
})
export class GamesContainerComponent {
  public categoriesList: any[];
  public categoriesList$: Observable<any>;
  public gamesList$: Observable<any>;
  public merchantsList$: Observable<any>;
  public currentPage$: Observable<number>;
  public searchGamesList;
  public filterFields: IDynamicFilterField[];
  public filterForm: FormGroup;
  public sortControls: FormGroup;
  private sortDirections: SortDirection[] = [
    SortDirection.ASC,
    SortDirection.DESC,
  ];

  constructor(private route: ActivatedRoute) {
    this.currentPage$ = this.route.queryParams.pipe(
      pluck('page'),
      map((page) => page || 1),
    );
    this.categoriesList$ = this.route.data.pipe(
      pluck('gamesList', 'categories'),
    );
    this.gamesList$ = this.route.data.pipe(pluck('gamesList', 'games'));
    this.merchantsList$ = this.route.data.pipe(pluck('gamesList', 'merchants'));
    this.createForm();
    this.initFilterFields();
    this.categoriesList$.subscribe((categoriesList: any[]) => {
      this.categoriesList = categoriesList;
      this.filterFields.push({
        fieldLabel: 'Categories',
        fc: this.filterForm.get('categoryID') as FormControl,
        isMain: false,
        type: DynamicFilterFieldType.SELECT,
        select: {
          isAsync: false,
          isMultiple: true,
          isCloseOnSelect: true,
          isHideSelected: true,
          options: this.categoriesOptions,
        },
      });
    });
  }

  public gamesListUpdate(search) {
    this.searchGamesList = search;
  }

  private createForm() {
    this.sortControls = new FormGroup({
      sortAlphabetic: new FormControl(null),
    });
    this.filterForm = new FormGroup({
      name: new FormControl(null),
      categoryID: new FormControl(null),
      merchant: new FormControl(null),
      ...this.sortControls.controls,
    });
  }

  private initFilterFields() {
    this.filterFields = [
      {
        class: 'col_big',
        fieldLabel: 'Search by name',
        fc: this.filterForm.get('name') as FormControl,
        isMain: true,
        type: DynamicFilterFieldType.TEXT,
      },
      {
        fieldLabel: 'Sort by name',
        fc: this.filterForm.get('sortAlphabetic') as FormControl,
        isMain: true,
        isSort: true,
        placeholder: this.sortDirectionsOptions[0].label,
        type: DynamicFilterFieldType.SELECT,
        select: {
          isAsync: false,
          isMultiple: false,
          isCloseOnSelect: true,
          isHideSelected: true,
          options: this.sortDirectionsOptions,
        },
      },
      {
        fieldLabel: 'Merchant',
        fc: this.filterForm.get('merchant') as FormControl,
        isMain: false,
        type: DynamicFilterFieldType.TEXT,
      },
    ];
  }

  private get categoriesOptions() {
    return this.categoriesList.map((category) => {
      return {
        label: category.Name.en,
        value: category.ID,
      };
    });
  }

  private get sortDirectionsOptions() {
    return this.sortDirections.map((item) => {
      return {
        label: item,
        value: item,
        isNeedTranslate: true,
      };
    });
  }
}
