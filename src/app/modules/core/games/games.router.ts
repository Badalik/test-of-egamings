import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { BaseLayoutComponent } from '@shared/layout';
import { GamesContainerComponent } from './containers';
import { GamesListResolver } from './resolvers';

const routes: Route[] = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      {
        path: '',
        component: GamesContainerComponent,
        resolve: {
          gamesList: GamesListResolver,
        },
      },
      {
        path: 'category/:categoryId',
        component: GamesContainerComponent,
        resolve: {
          gamesList: GamesListResolver,
        },
      },
    ],
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class GamesRouting {}
