import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { GamesListQueryControllerService } from '../../services';

@Injectable({
  providedIn: 'root',
})
export class GamesListResolver implements Resolve<any> {
  constructor(private gamesListQuery: GamesListQueryControllerService) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.gamesListQuery.fetchGamesList();
  }
}
