import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BaseLayoutComponent, HeaderComponent } from './components';

@NgModule({
  declarations: [BaseLayoutComponent, HeaderComponent],
  imports: [CommonModule, RouterModule],
})
export class LayoutModule {}
