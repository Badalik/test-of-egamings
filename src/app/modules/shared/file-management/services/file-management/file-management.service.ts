import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FileManagementService {
  constructor(private http: HttpClient) {}

  public extractImage(imagePath: string = '') {
    return this.http.get(imagePath, {
      responseType: 'blob',
    });
  }
}
