import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { AngularDropdownModule } from 'angular-dropdown';

import { FormModule } from '@shared/form';
import { PipesModule } from '@shared/pipes';
import {
  FiltersDynamicComponent,
  FiltersDynamicFieldComponent,
  GamesListFilterComponent,
} from './components';

@NgModule({
  declarations: [
    GamesListFilterComponent,
    FiltersDynamicComponent,
    FiltersDynamicFieldComponent,
  ],
  imports: [
    CommonModule,
    FormModule,
    ReactiveFormsModule,
    AngularDropdownModule,
    NgSelectModule,
    PipesModule,
  ],
  exports: [GamesListFilterComponent, FiltersDynamicComponent],
})
export class FiltersModule {}
