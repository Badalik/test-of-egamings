import { FormControl } from '@angular/forms';
import { IDynamicFilterFieldSelect } from '../';
import { DynamicFilterFieldType } from '../../enums';

export interface IDynamicFilterField {
  class?: string;
  fieldLabel: string;
  isMain: boolean;
  type: DynamicFilterFieldType;
  fc: FormControl;
  placeholder?: string;
  isSort?: boolean;
  select?: IDynamicFilterFieldSelect;
  name?: string;
  trackIfOpen?: boolean;
  trackIfSearch?: boolean;
}
