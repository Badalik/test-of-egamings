export interface IDynamicFilterFieldSelectOption {
  label: string;
  value: any;
}
