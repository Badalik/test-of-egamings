import { Observable } from 'rxjs';
import { IDynamicFilterFieldSelectOption } from '../dynamic-filter-field-select-option';

export interface IDynamicFilterFieldSelect {
  isMultiple: boolean;
  isCloseOnSelect?: boolean;
  isHideSelected?: boolean;
  isAsync?: boolean;
  options?: IDynamicFilterFieldSelectOption[];
  optionsAsync?: Observable<IDynamicFilterFieldSelectOption[]>;
}
