export enum DynamicFilterFieldType {
  TEXT = 'text',
  NUMBER = 'number',
  SELECT = 'select',
}
