import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DynamicFilterFieldType } from '@shared/filters/enums';
import { IDynamicFilterField } from '@shared/filters/models';

@Component({
  selector: 'filters-dynamic-field',
  templateUrl: './filters-dynamic-field.component.html',
})
export class FiltersDynamicFieldComponent {
  @Input() public field: IDynamicFilterField;
  @Output() public openSelectField: EventEmitter<void> = new EventEmitter();
  @Output() public searchSelectField: EventEmitter<any> = new EventEmitter();

  public get isSelect(): boolean {
    return this.field.type === DynamicFilterFieldType.SELECT;
  }

  public onOpenSelectField() {
    if (this.field.trackIfOpen && this.openSelectField.observers.length > 0) {
      this.openSelectField.emit();
    }
  }

  public onSearchSelectField(value: any) {
    if (
      this.field.trackIfSearch &&
      this.searchSelectField.observers.length > 0
    ) {
      this.searchSelectField.emit(value);
    }
  }
}
