import { Component, EventEmitter, Input, Output } from '@angular/core';

import { DynamicFilterFieldType } from '../../enums';
import { IDynamicFilterField } from '../../models';

@Component({
  selector: 'filters-dynamic',
  templateUrl: './filters-dynamic.component.html',
  styleUrls: ['./filters-dynamic.component.scss'],
})
export class FiltersDynamicComponent {
  @Input() public fields: IDynamicFilterField[];
  @Output() public openSelectField: EventEmitter<
    IDynamicFilterField
  > = new EventEmitter();
  @Output() public searchSelectField: EventEmitter<{
    field: IDynamicFilterField;
    value: any;
  }> = new EventEmitter();
  public dynamicFilterFieldType = DynamicFilterFieldType;

  public get mainFields(): IDynamicFilterField[] {
    return this.fields ? this.fields.filter((field) => field.isMain) : [];
  }

  public get secondaryFields(): IDynamicFilterField[] {
    return this.fields ? this.fields.filter((field) => !field.isMain) : [];
  }

  public onOpen(selectField: IDynamicFilterField) {
    this.openSelectField.emit(selectField);
  }

  public onSearch(selectField: IDynamicFilterField, value: any) {
    this.searchSelectField.emit({ field: selectField, value });
  }
}
