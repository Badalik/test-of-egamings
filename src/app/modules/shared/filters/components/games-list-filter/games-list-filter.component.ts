import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { IGamesFilterQuery } from '@backend-bridge/games-view';
import { IDynamicFilterField } from '@shared/filters/models';

import { ReplaySubject } from 'rxjs';
import { auditTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'games-list-filter',
  templateUrl: './games-list-filter.component.html',
})
export class GamesListFilterComponent implements OnInit, OnDestroy {
  @Input() public filterFields: IDynamicFilterField[];
  @Input() public filterForm: FormGroup;
  @Input() public sortControls: FormGroup;
  @Output() public searchUpdate = new EventEmitter<IGamesFilterQuery>();

  private destroy$: ReplaySubject<boolean> = new ReplaySubject(1);
  private sortFields = {
    sortAlphabetic: 'Name',
  };
  private sortParams = {
    sortDirection: null,
    sortField: null,
  };

  constructor(private router: Router, private route: ActivatedRoute) {}

  public ngOnInit() {
    this.fillForm();

    this.route.queryParams.subscribe((queryParams) => {
      if (!Object.keys(queryParams).length) {
        this.filterForm.reset();
      }
    });

    this.filterForm.valueChanges
      .pipe(auditTime(400), takeUntil(this.destroy$))
      .subscribe((rawValue) => {
        const search = { ...rawValue };
        this.updateFilter(search);
      });

    Object.keys(this.sortControls.controls).forEach((formControlName) => {
      this.sortControls.controls[formControlName].valueChanges
        .pipe(distinctUntilChanged())
        .subscribe((value) => {
          this.createSortParams(formControlName, value);
        });
    });
  }

  public updateFilter(rawSearch) {
    const search = {
      ...{ ...this.removeExcessParams(rawSearch), ...this.sortParams },
    };
    this.searchUpdate.emit(search);
    this.router.navigate([], {
      queryParams: search,
      queryParamsHandling: '',
    });
  }

  public ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  private fillForm() {
    const params = { ...this.route.snapshot.queryParams };
    if (params.sortDirection && params.sortField) {
      Object.keys(this.sortFields).some((key) => {
        const isKeySought = this.sortFields[key] === params.sortField;
        if (isKeySought) {
          params[key] = params.sortDirection;
        }
        return isKeySought;
      });
    }
    this.filterForm.reset();
    if (params.categoryID && !Array.isArray(params.categoryID)) {
      params.categoryID = [params.categoryID];
    }
    this.filterForm.patchValue(params);
    this.searchUpdate.emit(params);
  }

  private createSortParams(formControlNameParam: string, value: string) {
    const params = {};
    Object.keys(this.sortControls.controls).forEach((formControlName) => {
      if (formControlName === formControlNameParam) {
        params[formControlName] = value;
      } else {
        params[formControlName] = null;
      }
    });
    this.sortParams.sortDirection = value;
    this.sortParams.sortField = value
      ? this.sortFields[formControlNameParam]
      : null;
    this.filterForm.patchValue(params, { emitEvent: false });
  }

  private removeExcessParams(params) {
    const obj = {};
    Object.keys(params).forEach((key) => {
      if (!params[key]) {
        obj[key] = null;
      } else if (typeof this.sortFields[key] === 'undefined') {
        obj[key] = params[key];
      }
    });
    return obj;
  }
}
