import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toString',
})
export class ToStringPipe implements PipeTransform {
  public transform(value: any): string {
    const result = value !== null && value !== undefined ? String(value) : '';
    return result;
  }
}
