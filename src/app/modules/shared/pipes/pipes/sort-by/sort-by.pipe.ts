import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sortBy' })
export class SortByPipe implements PipeTransform {
  private static getValueByArrayIndex(item, field?: string | string[]) {
    if (Array.isArray(field)) {
      return field.reduce((prev, current) => {
        return prev[current];
      }, item);
    } else {
      return item[field];
    }
  }

  private types: RegExp[] = [
    /^\d+$/,
    /^[A-Za-z0-9][-_.A-Za-z0-9\s]+$/,
    /^[а-яА-ЯёЁ0-9][-_.A-Za-zа-яА-ЯёЁ0-9\s]+$/,
  ];

  public transform(array: any[], field?: string | string[]) {
    const numericArr: number[] = [];
    const latinArr: string[] = [];
    const cyrillicArr: string[] = [];
    const otherArr: string[] = [];
    const compareArr: any[] = [numericArr, latinArr, cyrillicArr, otherArr];

    if (array && array.length) {
      this.typeSort(array, compareArr, field);
      numericArr.sort((a: number, b: number) => {
        return this.compareNum(a, b, field);
      });
      latinArr.sort((a: any, b: any) => {
        return this.compareSymbol(a, b, field);
      });
      cyrillicArr.sort((a: any, b: any) => {
        return this.compareSymbol(a, b, field);
      });
      otherArr.sort((a: any, b: any) => {
        return this.compareSymbol(a, b, field);
      });
      return [...numericArr, ...latinArr, ...cyrillicArr, ...otherArr];
    } else {
      return array;
    }
  }

  private typeSort(array: any[], compareArr: any[], field?: string | string[]) {
    array.forEach((item) => {
      const target = field
        ? SortByPipe.getValueByArrayIndex(item, field)
        : item;
      const isValidItem = this.types.some((regExp, i) => {
        const test = regExp.test(target);
        if (test) {
          compareArr[i].push(item);
        }
        return test;
      });
      if (!isValidItem) {
        compareArr[3].push(item);
      }
    });
  }

  private compareNum(a: any, b: any, field?: string | string[]): number {
    a = field ? Number(SortByPipe.getValueByArrayIndex(a, field)) : Number(a);
    b = field ? Number(SortByPipe.getValueByArrayIndex(b, field)) : Number(b);
    if (a > b) {
      return 1;
    }
    if (a < b) {
      return -1;
    }
  }

  private compareSymbol(a: any, b: any, field?: string | string[]) {
    a = field ? SortByPipe.getValueByArrayIndex(a, field) : a.toLowerCase();
    b = field ? SortByPipe.getValueByArrayIndex(b, field) : b.toLowerCase();
    if (a < b) {
      return -1;
    } else if (a > b) {
      return 1;
    } else {
      return 0;
    }
  }
}
