import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FileManagementService } from '@shared/file-management';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'objectUrlImageExtractor',
  pure: true,
})
export class ObjectUrlImageExtractorPipe implements PipeTransform {
  constructor(
    private fileService: FileManagementService,
    private sanitizer: DomSanitizer,
  ) {}

  public transform(imagePath: string, isBackground: boolean = false) {
    return this.fileService.extractImage(imagePath).pipe(
      map((image) => {
        let imageObject;
        const imageBlob: Blob = new Blob([image], { type: 'image/jpeg' });
        if (isBackground) {
          imageObject = `url(${window.URL.createObjectURL(imageBlob)})`;
          return this.sanitizer.bypassSecurityTrustStyle(imageObject);
        } else {
          imageObject = window.URL.createObjectURL(imageBlob);
          return this.sanitizer.bypassSecurityTrustResourceUrl(imageObject);
        }
      }),
    );
  }
}
