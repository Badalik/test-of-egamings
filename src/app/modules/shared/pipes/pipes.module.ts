import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ObjectUrlImageExtractorPipe, SortByPipe, ToStringPipe } from './pipes';

@NgModule({
  declarations: [ObjectUrlImageExtractorPipe, SortByPipe, ToStringPipe],
  imports: [CommonModule],
  exports: [ObjectUrlImageExtractorPipe, SortByPipe, ToStringPipe],
})
export class PipesModule {}
