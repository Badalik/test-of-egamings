import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'form-ng-select',
  templateUrl: './form-ng-select.component.html',
  styleUrls: ['./form-ng-select.component.scss'],
})
export class FormNgSelectComponent {
  @Input() public bindValue: string = 'value';
  @Input() public bindLabel: string = 'label';
  @Input() public closeOnSelect: boolean;
  @Input() public fc: FormControl;
  @Input() public hideSelected: boolean;
  @Input() public items: any[];
  @Input() public labelForId: string;
  @Input() public multiple: boolean;
  @Input() public placeholder: string;
  @Input() public title: string;
  @Output() public valueChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() public valueClear: EventEmitter<void> = new EventEmitter<void>();
  @Output() public open: EventEmitter<void> = new EventEmitter<void>();
  @Output() public close: EventEmitter<void> = new EventEmitter<void>();
  @Output() public search: EventEmitter<any> = new EventEmitter<any>();

  public change(item: any) {
    const value = item ? item[this.bindValue] : null;
    this.valueChange.emit(value);
  }

  public clear() {
    this.valueClear.emit();
  }

  public onOpen() {
    this.open.emit();
  }

  public onSearch(value: any) {
    this.search.emit(value.term);
  }

  public onClose() {
    this.close.emit();
  }
}
