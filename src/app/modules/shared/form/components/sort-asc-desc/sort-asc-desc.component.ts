import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SortDirection } from '@backend-bridge/base-bridge';

@Component({
  selector: 'form-sort-asc-desc',
  templateUrl: './sort-asc-desc.component.html',
  styleUrls: ['./sort-asc-desc.component.scss'],
})
export class SortAscDescComponent {
  @Input() public fc: FormControl;
  @Input() public title: string;

  public onChange() {
    if (this.fc.value === SortDirection.DESC || !this.fc.value) {
      this.fc.patchValue(SortDirection.ASC);
    } else {
      this.fc.patchValue(SortDirection.DESC);
    }
  }
}
