import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { PipesModule } from '@shared/pipes';
import { FormNgSelectComponent, SortAscDescComponent } from './components';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NgSelectModule, PipesModule],
  declarations: [FormNgSelectComponent, SortAscDescComponent],
  exports: [FormNgSelectComponent, SortAscDescComponent],
})
export class FormModule {}
